---
id: notifications
title: Notifications
---

## Web Push

Web Push Notification require VAPID keys. See [https://github.com/web-push-libs/web-push-php#authentication-vapid](https://github.com/web-push-libs/web-push-php#authentication-vapid) to learn how to set these up.