---
id: did
title: DID
---

> This is a work in progress. See https://gitlab.com/groups/minds/-/epics/211 for more information about what we are working on.

## The Minds DID

In order to give every Minds user a DID, we have opted to introduce the [did:web method](https://w3c-ccg.github.io/did-method-web/). Other methods may be considered in the future.

An example of a DID:
```json
{
  "@context":"https://www.w3.org/ns/did/v1",
  "id":"did:web:feat-2292-did.minds.io:mark",
  "verificationMethod":[
    {
      "id":"did:web:feat-2292-did.minds.io:mark#key-1",
      "type":"Ed25519VerificationKey2020",
      "controller":"did:web:feat-2292-did.minds.io:mark",
      "publicKeyMultibase":"mwU0pSAjd7CQ7lXyr+9IlkXU1Oycj8NFH/AoIhBNQPpE="
    }
  ],
  "authentication":[
    "did:web:feat-2292-did.minds.io:mark#key-1"
  ]
}
```

### How it works

See [https://w3c-ccg.github.io/did-method-web/](https://w3c-ccg.github.io/did-method-web/).

#### Resolver example

[did:web:minds.com:mark](https://minds.com/mark/did.json) -> https://minds.com/mark/did.json

### Linking other DID's

> We are currently working on this section

The DID Spec includes the [alsoKnownAs](https://www.w3.org/TR/did-core/#dfn-alsoknownas) property. This can be used to link multiple DID's together. For example:

did:web:minds.com:mark:
```json
{
  ...
  "alsoKnownAs": [ "did:ethr:0x177fd9efd24535e73B81E99e7f838cDeF265e6Cb" ],
}
```

did:ethr:0x177fd9efd24535e73B81E99e7f838cDeF265e6Cb
```json
{
  ...
  "alsoKnownAs": [ "did:web:minds.com:mark" ],
}
```

As both DID documents above reference each other, we can safely assume the intent to link the DID's.

## Verification and Authentication

The DID Spec allows for optional [verification](https://www.w3.org/TR/did-core/#verification-methods) and [authentication](https://www.w3.org/TR/did-core/#authentication) properties.

Minds.com may sign entities on users behalfs using the DID keypairs. These keypairs should be considered as temporary credentials and may be regularly rotated. 