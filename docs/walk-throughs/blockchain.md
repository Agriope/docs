---
id: blockchain
title: Blockchain
---

# SKALE

> **Important!**
> This feature is currently in development and is currently in a work in progress state

## Overview

The SKALE minds system is a custodial wallet system that acts as a replacement to the "offchain" Minds system.
Tokens are transacted between wallets internally, simplifying UX significantly. This guide focuses on development on this system.

## Setting up Metamask

To set up on testnet, add a new network to Metamask with the below details:

- Name: `SKALE Testnet (rapping-zuben-elakrab)`
- RPC URL: `https://staging-v2.skalenodes.com/v1/rapping-zuben-elakrab`
- Chain ID: `0x4a393bf89c676`
- Currency Symbol: `sFuel`

Then import a new token, with the address `0x86f5Fd15235C58fb4d250b3674e6ad252e5bB796` for whatever wallet you are using.

It can be helpful to import wallets from the sandbox into your Metamask - when you do this you may need to re-add the token. More on how to do this below under [Printing Wallet Info](#printing-wallet-info).

## Configuration

The following keys can be lifted from the `settings.example.php`, located in the `blockchain` section of the config. Explanations for different values:

- `rpc_endpoints`

Array of RPC endpoints - only the first entry is currently used. Other entries in future will work as backups.

- `development_mode`

Whether to run in development mode - will allow output of private keys.

- `minds_token_address` 

Token address of MINDS token on SKALE network.

- `gas_price_wei`

Gas price in Wei - the value of this is fixed for a chain and doesn't vary like gas price in the Ethereum network, but varies from SKALE chain to chain. At the time of writing, `100000` Wei is correct for `whispering-turais`.

- `default_sfuel_distributor_guid`

The GUID of the user delegated as the sFuel distributor. The purpose of this user is to have a linked wallet that holds reserves of sFuel - sFuel can be requested from wallets and distributed from this users balance when a wallet does not hold enough sFuel to make a transaction. If using the `Tools` class in PHP (called by the CLI), distribution will happen automatically when a custodial wallet attempts to make a token transfer without enough sFuel, prior to attempting the transaction.

- `default_sfuel_distribution_amount_wei`

The amount of sFuel that should be distributed when a request for sFuel distribution is made (unless manually overrode, such as by the CLI).

- `sfuel_low_threshold`

The value in Wei that constitutes a low sFuel balance - when a wallet crosses below this threshold, if they attempt to make a token transfer, sFuel will be distributed to their address prior to the transfer being attempted.

- `confirmation_timeout_seconds`

When a transaction is made, confirmation of the transaction onchain is awaited. This value controls the maximum time that the server will wait for a transaction before it stops listening.

- `confirmation_polling_gap_seconds`

The gap in between polling attempts to check for transaction completion.

- `balance_cache_ttl_seconds`

How long in seconds the TTL should be for storage of sFuel and MINDS (SKALE) balances in cache.

## Useful Tools

- Testnet Block Explorer: [https://rapping-zuben-elakrab.explorer.staging-v2.skalenodes.com/](https://rapping-zuben-elakrab.explorer.staging-v2.skalenodes.com/)
- Ethereum Unit Converter: https://eth-converter.com/

## CLI

Minds CLI tool can be used to interact with the SKALE chain in various ways.

### Generate Wallet for User

To generate a wallet for a user, you need to run in the CLI below - if development mode is enabled, the private key will be printed.

```bash
php cli.php Skale generateKeys --username=userA
```

### Printing Wallet Info

Prints keys for a given user:

```bash
php cli.php Skale printKeys --username=userA
```

### Get Token Balance

You can request the token balance of the custodial wallet linked to a user, or any Ethereum address using one of the below commands:

```bash
php cli.php Skale getTokenBalance --username=userA
php cli.php Skale getTokenBalance --address=0x00....
```

### Get sFuel Balance

Similarly, you can request the sFuel balance of the custodial wallet linked to a user, or any Ethereum address:

```bash
php cli.php Skale getSFuelBalance --username=userA
php cli.php Skale getSFuelBalance --address=0x00000...
```

### Send Tokens

Tokens can be sent via the below commands.

```bash
# Between two custodial wallets
php cli.php Skale sendTokens --senderUsername=userA --receiverUsername=userB --amountWei=10000000000000000

# From a custodial wallet to an Ethereum address
php cli.php Skale sendTokens --senderUsername=userA --receiverAddress=0x00000... --amountWei=10000000000000000
```

Note that if the sender user does not have enough sFuel, an transaction will be sent to the wallet prior to dispatching the token transfer to give the wallet sFuel- confirmation of this will be awaited, so the command may take slightly longer.

### Send sFuel

sFuel can be sent manually using one of the below commands:

```bash
# Between two custodial wallets - will use default distribution amount
php cli.php Skale sendSFuel --senderUsername=userA --receiverUsername=userB

# From a custodial wallet to an Ethereum address - will use default distribution amount
php cli.php Skale sendSFuel --senderUsername=userA --receiverAddress=0x00000...

# Between two custodial wallets - custom distribution amount.
php cli.php Skale sendSFuel --senderUsername=userA --receiverUsername=userB --amountWei=100000000000
```
